# Video 7, 8
# Color filtering
# Blurring and smoothing

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
	# Get frame from webcam
	_, frame = cap.read()
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# Set lower and upper bound for color to be filtered
	# Set for red, values in hsv
	lower_red = np.array([150, 150, 150])
	upper_red = np.array([180, 255, 255])
	# Create mask
	mask = cv2.inRange(hsv, lower_red, upper_red)
	# Apply mask
	red = cv2.bitwise_and(frame, frame, mask=mask)

	# Kernel for blurring
	k_size = 15
	kernel = np.ones((k_size, k_size), np.float32) / k_size ** 2
	# image, combinations, kernel
	smoothed = cv2.filter2D(red, -1, kernel)
	blur = cv2.GaussianBlur(red, (k_size, k_size), 0)
	median = cv2.medianBlur(red, 15)
	
	#cv2.imshow("frame", frame)
	#cv2.imshow("mask", mask)
	#cv2.imshow("Color filtered", red)
	#cv2.imshow("Smoothed with filter2d", smoothed)
	#cv2.imshow("Gaussian Blur", blur)
	cv2.imshow("Median Blur", median)

	# Break out of the loop when q is pressed
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()
cap.release()