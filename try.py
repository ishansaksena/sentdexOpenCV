#Video 1
#Open and display images using opencv and matlab

import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread('images/taj.jpg', cv2.IMREAD_GRAYSCALE)

#cv2.imshow("image", img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

plt.imshow(img, cmap='gray', interpolation='bicubic')
plt.show()
#cv2.imwrite('name.png', img)