# Video 3
# Drawing and writing 

import numpy as np
import cv2

img = cv2.imread('images/rio.jpg', cv2.IMREAD_COLOR)

# Drawing shapes
# On which image, start, end, color, thickness
cv2.line(img, (0, 0), (100, 100), (200, 200, 200), 10)
cv2.rectangle(img, (100, 100), (200, 200), (100, 100, 100), 5)
# image, center, radius, color, width(-1 -> filled in)
cv2.circle(img, (200, 200), 50, (150, 0, 0), -1)

# Making a polygon
points = np.array([[10, 5], [20, 30], [50, 10]], np.int32)
# True -> Closed polygon
cv2.polylines(img, [points], True, (0, 255, 255), 3)

# Writing text
font = cv2.FONT_HERSHEY_SIMPLEX
#image, text, start-point, font, size, color, thickness, anti-aliasing
cv2.putText(img, 'We are 3/21 videos through', (0, 100), font, 1, (0,0,0), 1, cv2.LINE_AA)

cv2.imshow("image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()