# Video 2
# Use the video feed from the built in webcam

import cv2
import numpy as np

# The number is which webcam we want to use, 0 indexed
# Or replace 0 with video file name
cap = cv2.VideoCapture(0)

#Writing images to disk, prep
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

while True:
	# Return if the we couldn't get a capture from the feed
	ret, frame = cap.read()
	# Convert to grayscale
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	# Display frames
	cv2.imshow('frame', frame)
	cv2.imshow('gray', gray)

	# Write to disk
	out.write(frame)

	# Break out of the loop
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

out.release()
# Release camera so that other applications can use it
cap.release()
# Close windows
cap.destroyAllWindows()
